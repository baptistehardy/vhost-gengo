# VHost GenGo

A command-line app to easily and quickly create Apache virtual hosts, written in Go.

Only works on Linux.

### Installation
```bash
go get gitlab.com/baptistehardy/vhost-gengo
go build
```

### To do

Refer to the repository's issues.