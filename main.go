package main

import (
	"fmt"
	"github.com/urfave/cli"
	"log"
	"os"
	"os/exec"
)

func main() {
	app := cli.NewApp()
	app.Name = "VHost GenGo"
	app.Author = "Baptiste Hardy"
	app.Version = "0.1"
	app.Usage = "Create Apache virtual hosts"
	app.Description = "A CLI tool to quickly and easily set up Apache virtual hosts"

	app.Commands = []cli.Command{
		{
			Name:    "new",
			Aliases: []string{"n"},
			Usage:   "Create a new Apache virtual host",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "dir, d",
					Value: "",
					Usage: "Path to the directory",
				},
			},
			Action: func(c *cli.Context) error {
				name := c.Args().First()
				if name == "" {
					fmt.Println("Please give a name")
					return nil
				}

				directory := c.String("dir")
				if directory == "" {
					fmt.Println("Please enter a valid directory with --dir or -d")
					return nil
				}

				fmt.Printf("Creating %v...\n", name)

				conf, err := os.Create("/etc/apache2/sites-available/" + name + ".conf")
				if err != nil {
					log.Fatal(err)
					return nil
				}

				data := []byte("<VirtualHost " + name + ":80>\n" +
					"	ServerName " + name + "\n" +
					"	DocumentRoot " + directory + "\n" +
					"	<Directory " + directory + ">\n" +
					"		AllowOverride All\n" +
					"		Order Allow,Deny\n" +
					"		Allow from All\n" +
					"	</Directory>\n" +
					"	ErrorLog ${APACHE_LOG_DIR}/error.log\n" +
					"	CustomLog ${APACHE_LOG_DIR}/access.log combined\n" +
					"</VirtualHost>")

				if _, err = conf.Write(data); err != nil {
					log.Fatal(err)
					return nil
				}
				if err := conf.Close(); err != nil {
					log.Fatal(err)
					return nil
				}

				a2ensite := exec.Command("sudo", "a2ensite", name)
				_ = a2ensite.Run()

				reload := exec.Command("systemctl", "reload", "apache2")
				_ = reload.Run()

				fmt.Println("Created the '" + name + "' virtual host for " + directory)

				hosts, err := os.OpenFile("/etc/hosts", os.O_APPEND|os.O_WRONLY, 0777)
				if err != nil {
					log.Fatal(err)
					return nil
				}
				if _, err := hosts.Write(
					[]byte("\n#[" + name + "]\n" +
						"127.0.0.1	" + name)); err != nil {
					log.Fatal(err)
					return nil
				}
				if err := hosts.Close(); err != nil {
					log.Fatal(err)
					return nil
				}

				if err = exec.Command("xdg-open", "http://" + name).Start(); err != nil {
					log.Fatal(err)
					return nil
				}

				return nil
			},
		},
		{
			Name:    "list",
			Aliases: []string{"l"},
			Usage:   "List virtual hosts",
			Action: func(c *cli.Context) error {
				cmd := exec.Command("ls", "/etc/apache2/sites-available")
				out, err := cmd.Output()
				if err != nil {
					log.Fatalf(err.Error())
					return nil
				}
				fmt.Print(string(out))
				return nil
			},
		},
		{
			Name:	"remove",
			Aliases: []string{"r"},
			Usage:	"Delete a virtual host",
			Action: func(c *cli.Context) {
				//to implement
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
